# OpenML dataset: autos

https://www.openml.org/d/45080

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This data set consists of three types of entities: (a) the specification of an auto in terms of various characteristics, (b) its assigned insurance risk rating, (c) its normalized losses in use as compared to other cars. The second rating corresponds to the degree to which the auto is more risky than its price indicates. Cars are initially assigned a risk factor symbol associated with its price. Then, if it is more risky (or less), this symbol is adjusted by moving it up (or down) the scale. Actuarians call this process "symboling". A value of +3 indicates that the auto is risky, -3 that it is probably pretty safe.The third factor is the relative average loss payment per insured vehicle year. This value is normalized for all autos within a particular size classification (two-door small, station wagons, sports/speciality, etc), and represents the average loss per car per year.Note: Several of the attributes in the database could be used as a "class" attribute.Source: https://www.kaggle.com/datasets/toramky/automobile-dataset

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45080) of an [OpenML dataset](https://www.openml.org/d/45080). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45080/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45080/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45080/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

